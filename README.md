# Lab5 -- Integration testing

## BVA

Type: "budget", "luxury", "nonsense"

Plan: "minute", "fixed_price", "nonsense"

Distance: "-10", "0", "10", "1000", "nonsense"

Planed Distance: "-10", "0", "10", "1000", "nonsense"

Time: "-10", "0", "10", "1000", "nonsense"

Planed time: "-10", "0", "10", "1000", "nonsense"

InnoDiscount: "yes", "no", "nonsense"

## Decision table

| # | type | plan | distance | planed distance | time | planed time | inno discount | expected | actual |
|-| - | - | - | - | - | - | - | - | - |
|1| budget | minute | 10 | 10 | 10 | 10 | yes | 130.5 | 139.2|
|2| budget | minute | 10 | 10 | 11 | 10 | no | 150 | 165 |
|3| budget | fixed_price | 10 | 10 | 10 | 10 | yes | 113.1 | 108.75 |
|4| luxury | minute | 10 | 10 | 10 | 10 |  yes | 356.7 | 356.7 |
|5| luxury | nonsense | -5 | 10 | -5 | 10 | no | Invalid request | 166.66666666666666 |
|6| luxury | fixed_price | -1 | 10 | 10 | 10 | no | Invalid request | 166.66666666666666 | 
|7| luxury | minute | -5 | -5 | 10 | 10 | yes | Invalid Request | Invalid Request |
|8| luxury | minute | -5 | nonsense | 10 | 10 | yes | Invalid Request | Invalid Request |
|9| luxury | minute | 10 | nonsense | 10 | 10 | yes | Invalid Request | 356.7 |
|10| luxury | minute | 10 | 10 | 0 | 0 | yes | Invalid Request | 0 |
|11| luxury | minute | 10 | 10 | 1000 | -5 | yes | Invalid Request | 35670 |
|12| luxury | minute | 10 | 10 | -5 | 10 | yes | Invalid Request | Invalid Request |
|13| nonsense | nonsense | 10 | 10 | 10 | 10 | yes | Invalid Request | Invalid Request |
|14| nonsense | fixed_price | 10 | 10 | 10 | 10 | yes | Invalid Request | 108.75 |

## Failing test cases

| # | type | plan | distance | planed distance | time | planed time | inno discount | expected | actual |
|-| - | - | - | - | - | - | - | - | - |
|1| budget | minute | 10 | 10 | 10 | 10 | yes | 130.5 | 139.2 |
|2| budget | minute | 10 | 10 | 11 | 10 | no | 150 | 165 |
|3| budget | fixed_price | 10 | 10 | 10 | 10 | yes | 113.1 | 108.75 |
|5| luxury | nonsense | -5 | 10 | -5 | 10 | no | Invalid request | 166.66666666666666 |
|6| luxury | fixed_price | -1 | 10 | 10 | 10 | no | Invalid request | 166.66666666666666 | 
|9| luxury | minute | 10 | nonsense | 10 | 10 | yes | Invalid Request | 356.7 |
|10| luxury | minute | 10 | 10 | 0 | 0 | yes | Invalid Request | 0 |
|11| luxury | minute | 10 | 10 | 1000 | -5 | yes | Invalid Request | 35670 |
|14| nonsense | fixed_price | 10 | 10 | 10 | 10 | yes | Invalid Request | 108.75 |

## Bugs

- Bad calculation with budget and minute plan
- Nonsense parameter accepted as a plan for luxury type  
- Allowed negative values for distance, planed distance, time, planed time
- Bad calculation for luxury and minute plan 
- Bad calculation for fixed_price plan 



